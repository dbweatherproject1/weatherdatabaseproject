const express = require("express");
const mssql = require("mssql");
const con = require("./config.js");
const cors = require("cors");

const app = express();

// Apply cors middleware
app.use(cors());
app.use(express.json()); // Parse JSON request bodies

app.post("/api/intervalData", async (req, res) => {
  console.log("post request..");
  console.log("after request", req.body);
  const { city, startDay, startMonth, startYear, endDay, endMonth, endYear } =
    req.body;
  try {
    const intervalData = await new Promise((resolve, reject) => {
      con.query(
        `Select Temperature,Humidity,Precipitation, WindSpeed from historicalweather where CityName = '${city.city}' ANd Year between ${startYear} And ${endYear} ANd Month Between ${startMonth} And ${endMonth} And Day Between ${startDay} And ${endDay}`,
        // "Select * from Calamity",
        (err, intervalData) => {
          if (err) reject(err);
          // console.log("Fetch data", intervalData);
          resolve(intervalData);
        }
      );
    });
    res.json(intervalData);
  } catch (err) {
    console.warn("Error in getting Interval Data:", err);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.post("/api/specific", async (req, res) => {
  const { column, startYear, startDay, startMonth, endDay, endMonth, endYear } =
    req.body;
  console.log("Enter Specific Api");
  try {
    const gotData = new Promise((rej, res) => {
      con.query(
        `Select ${column} from historicalweather where Year between ${startYear} And ${endYear} ANd Month Between ${startMonth} And ${endMonth} And Day Between ${startDay} And ${endDay}`,
        (err, specificData) => {
          if (err) rej(err);
          console.log("Fetched Specifice Data", specificData);
          res(specificData);
        }
      );
    });
    res.json(gotData);
  } catch (err) {
    console.warn("Error fetching specific data");
  }
});

const port = 5000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
